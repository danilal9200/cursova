<?php

class ClientsCest
{
    public function _before(FunctionalTester $I)
    {
    }

    // tests
    public function testClientsInGroup(FunctionalTester $I)
    {
        $I->amOnPage('/?group_id=POOL');
        $I->canSee('Phelps');
    }

    // tests
    public function testgetClientBySurname(FunctionalTester $I)
    {
        $I->haveInDatabase('clients',
            array('client_id' => '10',
                'Surname' => 'Zhuravel',
                'Sex' => '1',
                'YearsInFitness' => '10',
                'type' => 'GYM',));

        $I->seeInDatabase('clients', ['Surname' => 'Phelps']);
    }

}
