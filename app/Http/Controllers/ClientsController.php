<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clients;

class ClientsController extends Controller
{
   // public function list(Request $request) {
   //     return view ('app.clients.list');
  //  }

    public function index(Request $request){
        $type = $request->input('type', null);
        $model_clients = new Clients();
        $clients = $model_clients->getClients($type);
        return view('app.clients.list', [
                'clients' => $clients,
                'client_types' => Clients::$clients_types,
                'type_selected' => $type,]
        );
    }
    public function client($id){
        $model_clients = new Clients();
        $client = $model_clients->getClientByID($id);
        return view('app.clients.client')->with('client', $client);
    }


}
