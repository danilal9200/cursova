<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clients;
use App\Models\Client;
use Illuminate\Support\Facades\Redirect;

class AdminClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::get();
        return view('admin.client.list', ['clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.client.add', ['clients_types' => Clients::$clients_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $surname = $request->input('surname');
        $sex = $request->input('sex');
        $yearsInFitness = $request->input('yearsInFitness');
        $type = $request->input('type');

        $client = new Client();
        $client->surname = $surname;
        $client->sex = $sex;
        $client->yearsInFitness = $yearsInFitness;
        $client->type = Clients::$clients_types[$type];
        $client->save();
        return Redirect::to('/admin/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::where('client_id', $id)->first();
        return view('admin.client.edit', [
            'client' => $client,
            'clients_types' => Clients::$clients_types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::where('client_id', $id)->first();
        $client->surname = $request->input('surname');
        $client->sex = $request->input('sex');
        $client->yearsInFitness = $request->input('yearsInFitness');
        $client->type = $request->input('type');
        $client->type = Clients::$clients_types[$client->type] ;
        $client->save();
        return Redirect::to('/admin/clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::destroy($id);
        return Redirect::to('/admin/clients');
    }
}
