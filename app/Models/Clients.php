<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;

class Clients
{
    public function getClients($type){
        $query = DB::table('clients');
        $query->select('client_id', 'surname', 'sex', 'yearsInFitness', 'type')
            ->orderBy('surname');
        if($type){
            $query->where('type', '=', $type);
        }
        $clients = $query->get();
        return $clients;
    }

    public function getClientByID($id){
        if(!$id) return null;
        $client = DB::table('clients')
            ->select('*')
            ->where('client_id', $id)
            ->get()->first();
        return $client;
    }



    public static $clients_types = array(
        '1' => 'GYM',
        '2' => 'POOL',
        '3' => 'VIP'
);
}
