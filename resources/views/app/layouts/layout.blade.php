<html>
<head>
    <title>Інформаційна система "Фітнес-центр"</title>
</head>
<body>
@yield('page_title')
<br/><br/>
<div class="container">
    Основна інформація
    @yield('content')
</div>
<br/>
<br/>
</body>
@include('app.layouts.footer')
</html>
