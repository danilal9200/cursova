@extends('app.layouts.layout')
@section('page_title')
    <b>Список клієнтів</b>
@endsection
@section('content')
    <form method="get" action="/clients">
        <select name="type">
            <option value="0">всі категорії</option>
            @foreach($client_types as $type_id => $type_title)
                <option value="{{ $type_title }}"
                    {{ ( $type_title == $type_selected ) ? 'selected' : '' }}>
                    {{ $type_title }}
                </option>
            @endforeach
        </select>
        <input type="submit" value="Знайти" />
    </form>
    <table>
        <th>Прізвище</th>
        <th>Стать</th>
        <th>Роки в фітнесі</th>
        <th>Тип тренування</th>
        @foreach ($clients as $client)
            <tr>
                <td>
                    <a href="/clients/{{$client->client_id}}">
                        {{ $client->surname}}
                    </a>
                </td>
                <td>{{ $client->sex }}</td>
                <td>{{ $client->yearsInFitness }}</td>
                <td>{{ $client->type }}</td>
            </tr>
        @endforeach
    </table>
@endsection
