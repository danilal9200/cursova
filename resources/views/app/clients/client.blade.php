@extends('app.layouts.layout')
@section('page_title')
    <b>Інформація про клієнта {{ $client->surname }}</b>
@endsection
@section('content')
    <p>Прізвище - {{ $client->surname }}</p>
    <p>Стать - {{$client->sex}}</p>
    <p>Роки в залі - {{ $client->yearsInFitness }}</p>
    <p>Тип тренування - {{$client->type}}</p>
    <a href="/clients">Дивитися всіх клієнтів</a>
@endsection
