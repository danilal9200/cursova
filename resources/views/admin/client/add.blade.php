@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Додати клієнта</h2>
    <form action="/admin/clients" method="POST">
        {{ csrf_field() }}
        <label>Прізвище клієнта </label>
        <input type="text" name="surname">
        <br/><br/>
        <label>Стать</label>
        <input type="text" name="sex">
        <br/><br/>
        <label>Роки в фітнесі</label>
        <input type="text" name="yearsInFitness">
        <br/><br/>
        <label>Форма тренування</label>
        <select name="type">
            @foreach($clients_types as $type_id => $type_title)
                <option value="{{ $type_id }}">
                    {{ $type_title }}
                </option>
            @endforeach
        </select>
        <br/><br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
