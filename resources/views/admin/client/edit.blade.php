@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Редагування клієнта</h2>
    <form action="/admin/clients/{{ $client->client_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Прізвище</label>
        <input type="text" name="surname" value="{{$client->surname}}">
        <br/><br/>
        <label>Стать</label>
        <input type="text" name="sex" value="{{$client->sex}}">
        <br/><br/>
        <label>Роки в фітнессі</label>
        <input type="text" name="yearsInFitness" value="{{$client->yearsInFitness}}">
        <br/><br/>
        <label>Тип тренування</label>
        <select name="type">
            @foreach($clients_types as $type_id => $type_title)
                <option value="{{ $type_id }}"
                    {{ ( $type_id == $client->type ) ? 'selected' : '' }}>
                    {{ $type_title }}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
