@extends('admin.layout')
@section('content')
    <h2>Список клієнтів</h2>
    <table border="1"  style="text-align: center;">
        <th>Прізвище</th>
        <th>Стать</th>
        <th>Роки в фітнесі</th>
        <th>Тип тренування</th>
        <th>Дія</th>
        @foreach ($clients as $client)
            <tr>
                <td>
                  {{ $client->surname}}
                </td>
                <td>{{ $client->sex }}</td>
                <td>{{ $client->yearsInFitness }}</td>
                <td>{{ $client->type }}</td>
                <td>
                    <a href="/admin/clients/{{ $client->client_id }}/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/clients/{{ $client->client_id }}"method="POST">
                        {{ method_field('DELETE') }}

                        {{ csrf_field() }}
                        <button>Delete</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
