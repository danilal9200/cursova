<?php $__env->startSection('page_title'); ?>
    <b>Інформація про клієнта <?php echo e($client->surname); ?></b>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <p>Прізвище - <?php echo e($client->surname); ?></p>
    <p>Стать - <?php echo e($client->sex); ?></p>
    <p>Роки в залі - <?php echo e($client->yearsInFitness); ?></p>
    <p>Тип тренування - <?php echo e($client->type); ?></p>
    <a href="/clients">Дивитися всіх клієнтів</a>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\OpenServer\domains\fitnesscenter\resources\views/app/clients/client.blade.php ENDPATH**/ ?>