<h1>Адмінка</h1>
<link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right:
30px;">
    <b>Клієнти</b>
    <ul>
        <li><a href="/clients">список</a></li>
        <li><a href="/admin/clients/create">додати</a></li>
    </ul>
    <br/>
</div>
<div>
    <?php echo $__env->yieldContent('content'); ?>
</div>
<?php /**PATH D:\OpenServer\domains\fitnesscenter\resources\views/admin/layout.blade.php ENDPATH**/ ?>