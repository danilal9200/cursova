<?php $__env->startSection('page_title'); ?>
    <b>Список клієнтів</b>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <form method="get" action="/clients">
        <select name="type">
            <option value="0">всі категорії</option>
            <?php $__currentLoopData = $client_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type_id => $type_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($type_title); ?>"
                    <?php echo e(( $type_title == $type_selected ) ? 'selected' : ''); ?>>
                    <?php echo e($type_title); ?>

                </option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <input type="submit" value="Знайти" />
    </form>
    <table>
        <th>Прізвище</th>
        <th>Стать</th>
        <th>Роки в фітнесі</th>
        <th>Тип тренування</th>
        <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                    <a href="/clients/<?php echo e($client->client_id); ?>">
                        <?php echo e($client->surname); ?>

                    </a>
                </td>
                <td><?php echo e($client->sex); ?></td>
                <td><?php echo e($client->yearsInFitness); ?></td>
                <td><?php echo e($client->type); ?></td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\OpenServer\domains\fitnesscenter\resources\views/app/clients/list.blade.php ENDPATH**/ ?>