<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
<?php $__env->startSection('content'); ?>
    <h2>Редагування клієнта</h2>
    <form action="/admin/clients/<?php echo e($client->client_id); ?>" method="POST">
        <?php echo e(method_field('PUT')); ?>

        <?php echo e(csrf_field()); ?>

        <label>Прізвище</label>
        <input type="text" name="surname" value="<?php echo e($client->surname); ?>">
        <br/><br/>
        <label>Стать</label>
        <input type="text" name="sex" value="<?php echo e($client->sex); ?>">
        <br/><br/>
        <label>Роки в фітнессі</label>
        <input type="text" name="yearsInFitness" value="<?php echo e($client->yearsInFitness); ?>">
        <br/><br/>
        <label>Тип тренування</label>
        <select name="type">
            <?php $__currentLoopData = $clients_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type_id => $type_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($type_id); ?>"
                    <?php echo e(( $type_id == $client->type ) ? 'selected' : ''); ?>>
                    <?php echo e($type_title); ?>

                </option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\OpenServer\domains\fitnesscenter\resources\views/admin/client/edit.blade.php ENDPATH**/ ?>