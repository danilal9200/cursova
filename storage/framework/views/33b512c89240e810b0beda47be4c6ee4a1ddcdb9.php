<?php $__env->startSection('content'); ?>
    <h2>Список клієнтів</h2>
    <table border="1"  style="text-align: center;">
        <th>Прізвище</th>
        <th>Стать</th>
        <th>Роки в фітнесі</th>
        <th>Тип тренування</th>
        <th>Дія</th>
        <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                  <?php echo e($client->surname); ?>

                </td>
                <td><?php echo e($client->sex); ?></td>
                <td><?php echo e($client->yearsInFitness); ?></td>
                <td><?php echo e($client->type); ?></td>
                <td>
                    <a href="/admin/clients/<?php echo e($client->client_id); ?>/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/clients/<?php echo e($client->client_id); ?>"method="POST">
                        <?php echo e(method_field('DELETE')); ?>


                        <?php echo e(csrf_field()); ?>

                        <button>Delete</button>

                    </form>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\OpenServer\domains\fitnesscenter\resources\views/admin/client/list.blade.php ENDPATH**/ ?>