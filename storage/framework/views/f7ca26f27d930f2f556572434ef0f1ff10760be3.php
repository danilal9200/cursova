<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
<?php $__env->startSection('content'); ?>
    <h2>Додати клієнта</h2>
    <form action="/admin/clients" method="POST">
        <?php echo e(csrf_field()); ?>

        <label>Прізвище клієнта </label>
        <input type="text" name="surname">
        <br/><br/>
        <label>Стать</label>
        <input type="text" name="sex">
        <br/><br/>
        <label>Роки в фітнесі</label>
        <input type="text" name="yearsInFitness">
        <br/><br/>
        <label>Форма тренування</label>
        <select name="type">
            <?php $__currentLoopData = $clients_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type_id => $type_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($type_id); ?>">
                    <?php echo e($type_title); ?>

                </option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <br/><br/>
        <input type="submit" value="Зберегти">
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\OpenServer\domains\fitnesscenter\resources\views/admin/client/add.blade.php ENDPATH**/ ?>