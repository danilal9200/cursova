<html>
<head>
    <title>Інформаційна система "Фітнес-центр"</title>
</head>
<body>
<?php echo $__env->yieldContent('page_title'); ?>
<br/><br/>
<div class="container">
    Основна інформація
    <?php echo $__env->yieldContent('content'); ?>
</div>
<br/>
<br/>
</body>
<?php echo $__env->make('app.layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</html>
<?php /**PATH D:\OpenServer\domains\fitnesscenter\resources\views/app/layouts/layout.blade.php ENDPATH**/ ?>