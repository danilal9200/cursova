<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\AdminClientController;


Route::get('/', function () {
    return redirect('/clients');
});

Route::get('clients', [ClientsController::class, 'index']);
Route::get('clients/{client_surname}', [ClientsController::class, 'client']);

Route::resource('/admin/clients', AdminClientController::class)->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




